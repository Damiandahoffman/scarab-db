<?php
/*	
Scarab DB
Copyright (C) 2020  Damian Hoffman (Damian.da.hoffman@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	//wp_enqueue_style( 'rtl-style', get_template_directory_uri() . '/rtl.css' );
	wp_enqueue_script( 'x3dom', 'https://www.x3dom.org/release/x3dom.js', array(), '', true );
	wp_enqueue_script( 'scarab', get_stylesheet_directory_uri() . '/js/scarab.js', array(), '', true );
	wp_enqueue_script( 'media_upload', get_stylesheet_directory_uri() . '/js/default-media-uploader-view.js', array(), '', true );
	wp_enqueue_style( 'parent-style', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_script( 'jquery-ui', 'https://code.jquery.com/ui/1.10.4/jquery-ui.js', array(), '', true );
	//Fancybox
	wp_enqueue_script('fancybox', "https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js", array(), '', true );
	wp_enqueue_style( 'fancybox-style', "https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css");

}


//Allow Contributors to Add Media
if ( current_user_can('contributor') && !current_user_can('upload_files') )
add_action('admin_init', 'allow_contributor_uploads');

function allow_contributor_uploads() {
$contributor = get_role('contributor');
$contributor->add_cap('upload_files');
}

//wp_set_password( 'password', 3 );


function get_scarab_info($post_id)
{
	$scarab_info = array();
	$main_image_name	 = get_field( "Photos",$post_id );
	$scarab_info['main_image']= '<figure>';
	$scarab_info['main_image'].= '<img src = "'.$main_image_name['url'].'" alt = "'.$main_image_name['alt'].'"></img>';
	$scarab_info['main_image'].= '<figcaption>'.$main_image_name['description'].'</figcaption>';
	$scarab_info['main_image'].= '</figure>';	


	$scarab_info['description'] =  get_field_object( "description",$post_id );		
	$text = strip_tags($scarab_info['description']['value']);
    if (strlen($text) > 140) 
	{
        $scarab_info['excerpt'] = substr($text, 0, 140);
        $text = substr($scarab_info['excerpt'], 0, strrpos($scarab_info['excerpt']," "));
        $scarab_info['excerpt'] = $text. '...';
    }
	else
	{
		$scarab_info['excerpt'] = $text;
	}
	

	$scarab_info['Material'] = get_field_object('Material',$post_id);
	

	$image_gallery = acf_photo_gallery('Gallery', $post_id);
	if( count($image_gallery) )
	{
		$scarab_info['Gallery_array'] = array();
		foreach($image_gallery as $image)
		{
			$scarab_info['Gallery_array'][] = array('id'=> $image['id'], 'title'=>$image['title'],'url'=>$image['full_image_url'],'caption'=>$image['caption']);
		}
	}
	$scarab_info['Bibliography'] =  get_field( "Bibliography",$post_id );		
	$scarab_info['color'] = get_field_object( "color",$post_id );
	$scarab_info['period'] = get_field_object( "period",$post_id );
	$scarab_info['dynasty'] = get_field_object( "dynasty",$post_id );
	$scarab_info['length'] = get_field_object( "length",$post_id );
	$scarab_info['Width'] = get_field_object( "Width",$post_id );
	$scarab_info['Height'] = get_field_object( "Height",$post_id );
	$scarab_info['Weight'] = get_field_object( "Weight",$post_id );
	$scarab_info['type'] = get_field_object( "type",$post_id );
	$scarab_info['dynasty'] = get_field_object( "dynasty",$post_id );
	$scarab_info['surface'] = get_field_object( "surface",$post_id );
	
	$scarab_info["3dmodel_obj"] = get_field( "3dmodel_obj",$post_id );
	$scarab_info["3d_model_texture_file"] = get_field( "3d_model_texture_file",$post_id );

	
	$scarab_info["citation"] =  array(
		"first_name" => get_the_author_meta( 'first_name' ),
		"middle_name"=>get_the_author_meta( 'middle_name' ),
		"last_name" => get_the_author_meta( 'last_name' ),
		"site_name" => get_option('scarabDB_theme_options')['eng_name'],
		"item_title" => get_the_title(),
		"post_year" => get_the_date("Y"), 
		"post_month" => map_month_to_english(get_the_date("F")),
		"post_month_short" => map_month_to_english_short(get_the_date("F")), 
		"post_date" => get_the_date("d F Y"),
		"retrival_date" => date('d/m/Y', time()), 
		"retrival_date_harvard" => date('d', time()).' '.(date('F', time())).' '.date('Y', time()),
		"URL" => '<a href="'. get_permalink().'">'.get_permalink()."</a>",
	);
	return $scarab_info;

}


function print_citations($citation_info)
{
	
	$method = "Tel_Aviv";
	$citation_string  = "<pre class='citation_block'><strong>$method&#9;</strong><div id='$method'>";
	$citation_string .= strtoupper(substr($citation_info["last_name"],0,1)).substr($citation_info["last_name"],1). ", " .strtoupper(substr($citation_info["first_name"],0,1)) .". ";
	$citation_string .= $citation_info["post_year"].". ";
	$citation_string .= $citation_info["item_title"] .". <i>". $citation_info["site_name"] ."</i>, Accessed on ". $citation_info["retrival_date"] .",<br>". $citation_info["URL"]."</div>";
	$citation_string .="<button class='copy_citation' onclick=copy_citation('Tel_Aviv')>Copy</button></pre>";
	
	$method = 'MLA';
	$citation_string .= "<pre class='citation_block'><strong>$method&#9;</strong><div id='$method'>";
	$citation_string .= strtoupper(substr($citation_info["last_name"],0,1)).substr($citation_info["last_name"],1). ", " .(substr($citation_info["first_name"],0)) .". ";
	$citation_string .= '"'.$citation_info["item_title"].'"'.". <i>". $citation_info["site_name"] ."</i>. ";
	$citation_string .= $citation_info["post_month_short"]." ".$citation_info["post_year"].": ". $citation_info["URL"]."</div>";
	$citation_string .="<button class='copy_citation' onclick=copy_citation('MLA')>Copy</button></pre>";
	
	$method = 'APA';
	$citation_string .= "<pre class='citation_block'><strong>$method&#9;</strong><div id='$method'>";
	$citation_string .= strtoupper(substr($citation_info["last_name"],0,1)).substr($citation_info["last_name"],1). ", " .(strtoupper(substr($citation_info["first_name"],0,1))) .". ";
	$citation_string .= "(".$citation_info['post_year'].",".$citation_info['post_month'].")";
	$citation_string .= ' '.$citation_info["item_title"].'. '."<i>". $citation_info["site_name"] ."</i>. ";
	$citation_string .= '<br>Retrieved from '.$citation_info["URL"]."</div>";
	$citation_string .="<button class='copy_citation' onclick=copy_citation('APA')>Copy</button></pre>";
	
	$method = 'Harvard';
	$citation_string .= "<pre class='citation_block'><strong>$method&#9;</strong><div id='$method'>";
	$citation_string .= strtoupper(substr($citation_info["last_name"],0,1)).substr($citation_info["last_name"],1). ", " .(strtoupper(substr($citation_info["first_name"],0,1))) .". ";
	$citation_string .= "(".$citation_info['post_year']."). ";
	$citation_string .= '<i>'.$citation_info["item_title"].'</i> [Online]. '. $citation_info["site_name"] .". ";
	$citation_string .= 'Available at: '.$citation_info["URL"].'(Accessed: '. $citation_info["retrival_date_harvard"].").";
	$citation_string .= "</div>";
	$citation_string .="<button class='copy_citation' onclick=copy_citation('Harvard')>Copy</button></pre>";
	
	return $citation_string;
}


function my_theme_custom_upload_mimes( $existing_mimes ) {
	// add webm to the list of mime types
	$existing_mimes['stl'] = 'application/vnd.ms-pki.stl';
	$existing_mimes['x3d'] = 'model/x3d+xml';
	$existing_mimes['obj'] = 'text/plain';
	$existing_mimes['mtl'] = 'text/plain';
	// return the array back to the function with our added mime type
	return $existing_mimes;
}
add_filter( 'mime_types', 'my_theme_custom_upload_mimes' );


function wpcod_create_menu( $items, $args)
{
	if ($args->theme_location == 'primary') 
	{	
		//Login link
		$loginoutlink = wp_loginout('index.php',false);         
		$items .= '<li>'. $loginoutlink .'</li>'; 
	}
		return $items;

}
add_filter( 'wp_nav_menu_items', 'wpcod_create_menu', 10, 2 );


function remove_admin_bar() 
{
	if (!current_user_can('manage_options')) 
	{
		show_admin_bar(false);
	}
}
add_action('after_setup_theme', 'remove_admin_bar');


function remove_menus() 
{ 
	if (!current_user_can('manage_options'))
	{
		remove_menu_page( 'index.php' );                  //Dashboard
		remove_menu_page( 'jetpack' );                    //Jetpack* 
		remove_menu_page( 'edit.php' );                   //Posts
		remove_menu_page( 'upload.php' );                 //Media
		remove_menu_page( 'edit.php?post_type=page' );    //Pages
		remove_menu_page( 'edit-comments.php' );          //Comments
		remove_menu_page( 'themes.php' );                 //Appearance
		remove_menu_page( 'plugins.php' );                //Plugins
		remove_menu_page( 'users.php' );                  //Users
		remove_menu_page( 'tools.php' );                  //Tools
		remove_menu_page( 'options-general.php' );        //Settings
	}
}
add_action( 'admin_init', 'remove_menus' ,999);	

function remove_from_admin_bar($wp_admin_bar) {
    /*
     * Placing items in here will only remove them from admin bar
     * when viewing the fronte end of the site
    */
    if ( !current_user_can('manage_options') ) {
        // Example of removing item generated by plugin. Full ID is #wp-admin-bar-si_menu
        // WordPress Core Items (uncomment to remove)
        $wp_admin_bar->remove_node('updates');
        $wp_admin_bar->remove_node('comments');
        $wp_admin_bar->remove_node('new-content');
    }
 
    /*
     * Items placed outside the if statement will remove it from both the frontend
     * and backend of the site
    */
    $wp_admin_bar->remove_node('wp-logo');
}
add_action('admin_bar_menu', 'remove_from_admin_bar', 999);

function random_scarab_func( $atts ){
	$post_id = get_posts( array ( 'orderby' => 'rand', 'posts_per_page' => '1','post_type' => 'scarab','fields'=>'ids' ) );
	$main_image_name	 = get_field( "Photos",$post_id[0] );
	$scarab_info['main_image']= '<figure>';
	$scarab_info['main_image'].= '<h4>'.get_the_title($post_id[0]).'</h4>';
	$scarab_info['main_image'].= '<a href ="'.get_the_permalink($post_id[0]).'"><img src = "'.$main_image_name['url'].'" alt = "'.$main_image_name['alt'].'" width="400px"></img></a>';
	$scarab_info['main_image'].= '<figcaption>'.$main_image_name['description'].'</figcaption>';
	$scarab_info['main_image'].= '</figure>';
	return $scarab_info['main_image'];
	wp_reset_postdata();
}
add_shortcode( 'random_scarab', 'random_scarab_func' );



/**
 * Extend WordPress search to include custom fields
 *
 * https://adambalee.com
 */

/**
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where( $where ) {
    global $pagenow, $wpdb;

    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );

add_action('pre_get_posts','filter_media_per_post');

function filter_media_per_post( $wp_query_obj )
{
    global $current_user, $pagenow;

    $is_attachment_request = ($wp_query_obj->get('post_type')=='attachment');

    if( !$is_attachment_request )
        return;

    if( !is_a( $current_user, 'WP_User') )
        return;

    if( !in_array( $pagenow, array( 'upload.php', 'admin-ajax.php' ) ) )
        return;

    if( !current_user_can('delete_pages') )
        $wp_query_obj->set('post_parent', $_GET['post_id']);

    return;
}



add_action('admin_head', 'remove_image_fields');

function remove_image_fields() {
  print '
  <style>
        .media-sidebar .setting[data-setting="caption"],
        .media-sidebar .setting[data-setting="alt"],
		.media-sidebar .attachment-display-settings		
		{ 
            display: none; 
        }
  </style>';
}


function get_acf_archive_page()
{
	$args = [
    'post_type' => 'page',
    'fields' => 'ids',
    'nopaging' => true,
    'meta_key' => '_wp_page_template',
    'meta_value' => 'page-acfarchive.php'
	];
 $result = get_posts( $args );
 return $result[0];
}

function print_archivable_section($scarab_info,$field)
{
	if (!empty($scarab_info[$field]["value"]))
	{
		$acf_archive_page = get_the_permalink(get_acf_archive_page());
		print  '<div id = "scarab_'.$field.'"class="single_scarab_field">';
		print '<h2>'.  $scarab_info[$field]["label"] .'</h2>';
		if (is_array($scarab_info[$field]["value"])):
			foreach($scarab_info[$field]["value"] as $value):
				print '<a href="'. $acf_archive_page.'?'.$field.'='.$value.'">';							  
				print $value.'</a><br>';
			endforeach;
			print '</div>';
		else:
				$value = $scarab_info[$field]["value"];
				print '<a href="'. $acf_archive_page.'?'.$field.'='.$value.'">';							  
				print $value.'</a></div>';
		endif;
	}
}


function print_archivable_field($scarab_info,$field)
{
	if (!empty($scarab_info[$field]["value"]))
	{
		$acf_archive_page = get_the_permalink(get_acf_archive_page());
		if (is_array($scarab_info[$field]["value"])):
			foreach($scarab_info[$field]["value"] as $value):
				print '<a href="'. $acf_archive_page.'?'.$field.'='.$value.'">';							  
				print $value.'</a><br>';
			endforeach;				
		else:
				$value = $scarab_info[$field]["value"];
				print '<a href="'. $acf_archive_page.'?'.$field.'='.$value.'">';							  
				print $value.'</a>';
		endif;
	}
}

//enable php in text widget

add_filter('widget_text','execute_php',100);

function execute_php($html)
{
	 if(strpos($html,"<"."?php")!==false)
	 {
		 ob_start();
		 eval("?".">".$html);
		 $html=ob_get_contents();
		 ob_end_clean();
	 }
	 return $html;
}

function acf_tag_cloud($attr)
{
		$acf_field = "type";
		$args = array(
					'numberposts'	=> 1,
					'post_type'		=> 'scarab',
					);
		$custom_query = new WP_Query($args); 
		$field = get_field_object( $acf_field,$custom_query->posts[0]->ID );
		ob_start();
		foreach ($field['choices'] as $choice):
			$args = array(
						'numberposts'	=> -1,
						'post_type'		=> 'scarab',
						'meta_key'		=> $acf_field,
						'meta_value'	=> $choice
						);
			$custom_query = new WP_Query($args); 
			if ($custom_query->have_posts()):
				$scarab_info = get_scarab_info($custom_query->posts[0]->ID);
				print_archivable_field($scarab_info,$acf_field);
				print " (".$custom_query->found_posts.")";
				print "<br>";		
			else:
				print $choice;
				print " (0)";
				print "<br>";
			endif;
		endforeach;
		$output = ob_get_contents();
        ob_end_clean();

        return $output;
}

add_shortcode( 'acf_tag', 'acf_tag_cloud' );


/*function map_hebrew_month_to_english($month)
{
	$month_map = array(
 	"ינואר"  => "January",
	"פברואר" => "February",
	"מרץ"    => "March",
	"אפריל"  => "April",
	"מאי"    => "May",
	"יוני"   => "June",
	"יולי"   => "July",
	"אוגוסט" => "August",
	"ספטמבר" => "September",
	"אוקטובר"=> "October",
	"נובמבר" => "November",
	"דצמבר"  => "December",
	);
	return $month_map[$month];
	
}*/

function map_month_to_english($month)
{
	
	return $month;
	
}


/*function map_hebrew_month_to_english_short($month)
{
	$month_map = array(
 	"ינואר"  => "Jan.",
	"פברואר" => "Feb.",
	"מרץ"    => "Mar.",
	"אפריל"  => "Apr.",
	"מאי"    => "May.",
	"יוני"   => "Jun.",
	"יולי"   => "Jul.",
	"אוגוסט" => "Aug.",
	"ספטמבר" => "Sept.",
	"אוקטובר"=> "Oct.",
	"נובמבר" => "Nov.",
	"דצמבר"  => "Dec.",
	);
	return $month_map[$month];
	
}*/


function map_month_to_english_short($month)
{
	$month_map = array(
 	"January"=> "Jan.",
	"February"=> "Feb.",
	"March"=> "Mar.",
	"April"=> "Apr.",
	"May"=> "May.",
	"June"=> "Jun.",
	"July"=> "Jul.",
	"August"=> "Aug.",
	"September"=> "Sept.",
	"October"=> "Oct.",
	"November"=> "Nov.",
	"December"=> "Dec.",
	);
	return $month_map[$month];
	
}

add_filter(  'gettext',  'wps_translate_words_array'  );
add_filter(  'ngettext',  'wps_translate_words_array'  );
function wps_translate_words_array( $translated ) {
 
     $words = array(
            // 'word to translate' = > 'translation'
            'תיאור' => 'הערות',
            );
 
     $translated = str_ireplace(  array_keys($words),  $words,  $translated );
     return $translated;
}


add_action( 'customize_register', 'scarabDB_customize_register' );
function scarabDB_customize_register($wp_customize)
{   
    $wp_customize->add_section('scarabDB_setting', array(
        'title'    => __('scarabDB Settings', 'scarabDB'),
        'priority' => 10,
    ));
    //  =============================
    //  = Text Input                =
    //  =============================
    $wp_customize->add_setting('scarabDB_theme_options[eng_name]', array(
        'default'        => "Dayyan Stamp collection site",
        'capability'     => 'edit_theme_options',
        'type'           => 'option',
    ));
    $wp_customize->add_control('scarabDB_english_name', array(
        'label'      => __('English site name', 'scarabDB'),
        'section'    => 'scarabDB_setting',
        'settings'   => 'scarabDB_theme_options[eng_name]',
    ));
	
	$wp_customize->add_setting('scarabDB_theme_options[footer]', array(
        'default'        => "",
        'capability'     => 'edit_theme_options',
        'type'           => 'option',
    ));
    $wp_customize->add_control('scarabDB_footer', array(
        'label'      => __('Footer text', 'scarabDB'),
        'section'    => 'scarabDB_setting',
		'type'		 => 'textarea',	
        'settings'   => 'scarabDB_theme_options[footer]',
    ));
	
}


/**
 * Filters all menu item URLs for a #placeholder#.
 *
 * @param WP_Post[] $menu_items All of the nave menu items, sorted for display.
 *
 * @return WP_Post[] The menu items with any placeholders properly filled in.
 */
function scarab_dynamic_menu_items( $menu_items ) {

    // A list of placeholders to replace.
    // You can add more placeholders to the list as needed.
	// Possible extensions: Add logged in and permissions, change link text (mostly for the log in link)
    $placeholders = array(
			'#add_new#' => array(
            'url' => admin_url('post-new.php?post_type=scarab') ,
			),
			'#manage#'=> array(
			'url' => get_admin_url()."edit.php?post_type=scarab" ,
			),
      );
	
		foreach ( $menu_items as $key => $menu_item ) 
		{			
			if ( isset( $placeholders[ $menu_item->url ] ) ) 
			{
				if ( current_user_can('contributor') || current_user_can('administrator') )
				{
					if ( isset( $placeholders[ $menu_item->url ]['url'] ) ) 
					{
						
						$menu_item->url =$placeholders[ $menu_item->url ]['url'] ;
					}
				}
				else
				{
					unset($menu_items[$key]);
				}
			}
		}
    return $menu_items;
}
add_filter( 'wp_nav_menu_objects', 'scarab_dynamic_menu_items' );

add_action( 'ample_footer_copyright', 'scarab_footer_copyright', 10 );

function scarab_footer_copyright()
{
	echo  get_option('scarabDB_theme_options')['footer'];
}

?>