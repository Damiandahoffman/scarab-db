<?php
/**
* Template Name: page-acfarchive
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header();

   do_action( 'ample_before_body_content' ); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

   <div class="single-page clearfix">
      <div class="inner-wrap">
         <div id="primary">
            <div id="content">
               <?php
			   
			   parse_str($_SERVER['QUERY_STRING'],$result);
			   
			   if (empty($result)):
					$args = array(
								'numberposts'	=> -1,
								'post_type'		=> 'scarab',
								);
					$custom_query = new WP_Query($args);
					print '<div id="scarab_title">'.get_the_title().'</div>';
			   else:
				   
				   $args = array(
								'numberposts'	=> -1,
								'post_type'		=> 'scarab',
								'meta_key'		=> key($result),
								'meta_value'	=> $result[key($result)]
								);
					$custom_query = new WP_Query($args);   
					
					if (!$custom_query->have_posts()):
							$args = array(
										'numberposts'	=> -1,
										'post_type'		=> 'scarab',
										'meta_query' => array(array('key' => key($result),'value' => '"'.$result[key($result)].'"','compare' => 'LIKE'))
										);
							$custom_query = new WP_Query($args);   
					endif;
					if ($custom_query->have_posts()):
						$field = get_field_object(key($result), $custom_query->posts[0]->ID);
						print '<div id="scarab_title">'.$field['label'].': '. html_entity_decode($result[key($result)]).'</div>';				
					endif;
				endif;

		 ?>
				<table id="table_id" class="display">
					<thead>
						<tr>
							<th>Type</th>
							<th>Name</th>
							<th>Description</th>
							<th>Material</th>
							<th>Period</th>
							<th>Dynesty</th>
							<th>Surface Treatment</th>
							<th>Gallery</th>
						</tr>
					</thead>
					<tbody>
                  <?php while($custom_query->have_posts()) : $custom_query->the_post(); ?>
				  <?php $scarab_info = get_scarab_info(get_the_ID());?>
						<tr>
						<td><?php print_archivable_field($scarab_info,'type'); ?></td>
						<td><?php echo '<a href="' . get_permalink() . '">' . get_the_title() . '</a>'; ?></td>
						<td><?php print $scarab_info['excerpt'] ;?></td>
						<td><?php print_archivable_field($scarab_info,'Material');?></td>
						<td><?php print_archivable_field($scarab_info,'period'); ?></td>
						<td><?php print_archivable_field($scarab_info,'dynasty'); ?></td>
						<td><?php print_archivable_field($scarab_info,'surface'); ?></td>
						<td>
						<?php 
						$main_image_name	 = get_field( "Photos",get_the_ID() );
						print '<a href="'.$main_image_name['url'].'" data-fancybox="gallery" data-caption="'.$main_image_name['description'].'">';
						print '<img id="gallery_img" src = "'.$main_image_name['url'].'" alt = "'.$main_image_name['alt'].'"title="'.$main_image_name['description'].'"></img></a>';
						?>
						</td>
						</tr>
                  <?php endwhile; ?>
					</tbody>
				</table>
            </div>
         </div>
      </div><!-- .inner-wrap -->
   </div><!-- .single-page -->

   <?php do_action( 'ample_after_body_content' );
get_footer(); ?>
<script>



(jQuery)(document).ready(function() {
    // Setup - add a text input to each footer cell
    (jQuery)('#table_id thead th').each( function () {
        var title = (jQuery)(this).text();
        (jQuery)(this).html( '<input type="text" placeholder="Search" />'+title );
    } );
	var table = (jQuery)('#table_id').DataTable();
    // DataTable
    /*var table = (jQuery)('#table_id').DataTable({
		"language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Hebrew.json"
		}
	});*/
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        (jQuery)( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );
</script>



?>