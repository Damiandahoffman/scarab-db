// Three.js - Load .OBJ ?
// from https://threejsfundamentals.org/threejs/threejs-load-obj-no-materials.html


import * as THREE from 'https://threejsfundamentals.org/threejs/resources/threejs/r110/build/three.module.js';
import {OrbitControls} from 'https://threejsfundamentals.org/threejs/resources/threejs/r110/examples/jsm/controls/OrbitControls.js';
import {OBJLoader2} from 'https://threejsfundamentals.org/threejs/resources/threejs/r110/examples/jsm/loaders/OBJLoader2.js';
import {MTLLoader} from 'https://threejsfundamentals.org/threejs/resources/threejs/r115/examples/jsm/loaders/MTLLoader.js';
import {MtlObjBridge} from 'https://threejsfundamentals.org/threejs/resources/threejs//r115/examples/jsm/loaders/obj2/bridge/MtlObjBridge.js';
import { Material } from 'https://threejsfundamentals.org/threejs/resources/threejs/r110/build/three.module.js';

function main() {
  const canvas = document.querySelector('#c');
  const renderer = new THREE.WebGLRenderer({canvas});

  const fov = 90;
  const aspect = 2;  // the canvas default
  const near = 0.1;
  const far = 100;
  //const camera = new THREE.OrthographicCamera(10 / - 2, 10 / 2, 10 / 2, 10 / - 2, 1, 1000);
  //const    camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
	const camera = new THREE.PerspectiveCamera( 25, window.innerWidth / window.innerHeight, 1, 1000 );
camera.position.set( 0, 0, 100 );
//  camera.position.set(0, 10, 20);

  const controls = new OrbitControls(camera, canvas);
  controls.target.set(0, -5, 0);
  controls.update();

  const scene = new THREE.Scene();
  scene.background = new THREE.Color('black');
  scene.name="Seal";
  

  {
    const skyColor = 0xFFFFFF;  // light blue
    const groundColor = 0xFFFFFF;  // brownish orange
    const intensity = 3;
    const light = new THREE.HemisphereLight(skyColor, groundColor, intensity);
    scene.add(light);
  }

 
    const objLoader = new OBJLoader2();
	//var texture = new THREE.TextureLoader().load( 'http://3.127.97.174/seal_db/wp-content/uploads/2020/04/texture.jpg' );
	var texture = new THREE.TextureLoader().load( texture_url );

  
	const mtlLoader = new MTLLoader();
	/*mtlLoader.load('http://3.127.97.174/seal_db/wp-content/uploads/2020/04/materials.mtl', (mtlParseResult) => {
    var materials =  new MtlObjBridge.addMaterialsFromMtlLoader(mtlParseResult);	
	objLoader.addMaterials(materials);
  });*/
	
	
    objLoader.load( obj_url , function ( object ) {
                object.traverse( function ( child ) {				
                    if ( child instanceof THREE.Mesh ) {
                        child.material.map = texture;
                    }
                } );
                scene.add( object );
				var bbox = new THREE.Box3().setFromObject(object);
				const center = new THREE.Vector3();
				bbox.getCenter(center);
				// set camera to rotate around center of object
				controls.target = center;
            });


  function resizeRendererToDisplaySize(renderer) {
    const canvas = renderer.domElement;
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    const needResize = canvas.width !== width || canvas.height !== height;
    if (needResize) {
      renderer.setSize(width, height, false);
    }
    return needResize;
  }

  function render() {

    if (resizeRendererToDisplaySize(renderer)) {
      const canvas = renderer.domElement;
      camera.aspect = canvas.clientWidth / canvas.clientHeight;
      camera.updateProjectionMatrix();
    }

    renderer.render(scene, camera);

    requestAnimationFrame(render);
  }

requestAnimationFrame(render);
requestAnimationFrame(render);

}

main();
