<?php
/**
 * Theme Single Post Section for our theme.
 *
 * @package ThemeGrill
 * @subpackage Ample
 * @since Ample 0.1
 */

get_header();
do_action( 'ample_before_body_content' ); ?>

	<div class="single-page clearfix">
		<div class="inner-wrap">
			<div id="primary">
				<div id="content">

					<?php while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						   <?php do_action( 'ample_before_post_content' ); ?>
						
						   <?php ample_meta_select(); ?>
						   <?php $scarab_info = get_scarab_info(get_the_ID()); ?>
						   <div class="entry-content">
						   
					   
							<div id="scarab_title" class="single_scarab_field">
								<?php print the_title() ?>
							</div>
							 <div id = "scarab_image">
							  <?php 
								print $scarab_info['main_image'];	
							  ?>
							  </div>
							  
							  <div id = "scarab_citation" class="single_scarab_field">
									<a data-fancybox id="citation_button" data-options='{"src": "#fancybox-popup-form", "touch": false}' href="javascript:;">Citation</a>					
									<div class="hidden" id="fancybox-popup-form">
										<?php print print_citations($scarab_info["citation"]);?>
									</div>							 
								</div>	
							
							
								<?php print_archivable_section($scarab_info,'type'); ?>
							
							  <div id = "scarab_description" class="single_scarab_field">
							  <h2>  <?php print $scarab_info[ "description" ]['label'];?> </h2>
							  <?php print $scarab_info[ "description" ]['value'];?>		
							  </div>

							 							
							<?php print_archivable_section($scarab_info,'period'); ?>
							
							<?php print_archivable_section($scarab_info,'dynasty'); ?>					  		

							<?php print_archivable_section($scarab_info,'Material'); ?>
							  
							<?php print_archivable_section($scarab_info,'surface'); ?>
												   
							 <?php if (!empty($scarab_info['color']['value'])): ?>
							  <div id = "scarab_color" class="single_scarab_field">
							  <h2><?php print $scarab_info['color']['label'];?> </h2>
							  <input type="color" name="favcolor" disabled value="<?php print $scarab_info['color']['value'];?>"><br>
							  
							  <p id="scarab_color_name" style="text-align: right;display:inline" ></p><p style="text-align: right;display:inline" >(<?php print $scarab_info['color']['value'];?>)</p>
							  
							  <script type="text/javascript" src="http://chir.ag/projects/ntc/ntc.js"></script>

							  <script type="text/javascript">

							  var n_match  = ntc.name("<?php print $scarab_info['color']['value'];?>");
							  n_rgb        = n_match[0]; // RGB value of closest match
							  n_name       = n_match[1]; // Text string: Color name
							  n_exactmatch = n_match[2]; // True if exact color match

							  document.getElementById('scarab_color_name').innerText =n_name;

							</script>
							  
							  
							  
							  </div>		
							  <?php endif; ?>
							  
							  <div id = "scarab_measurments" class="single_scarab_field">
							  <h2> Measurments </h2>
							  <table id="scarab_measurments_table">
							  <tr><td><strong><?php print $scarab_info['length']['label'];?> :</strong></td><td><?php print $scarab_info['length']['value'];?> mm</td><tr>
							  <tr><td><strong><?php print $scarab_info['Width']['label'];?>:</strong></td><td><?php print $scarab_info['Width']['value'];?> mm</td><tr>
							  <tr><td><strong><?php print $scarab_info['Height']['label'];?>:</strong></td><td><?php print $scarab_info['Height']['value'];?> mm</td><tr>
							  <tr><td><strong><?php print $scarab_info['Weight']['label'];?>:</strong></td><td><?php print $scarab_info['Weight']['value'];?> mg</td><tr>
							  </table>
							  </div>		
						  

							  <div id = "scarab_Gallery_table" class="single_scarab_field">
							  <h2> Gallery</h2>
							  <table>
								<?php foreach ($scarab_info['Gallery_array'] as $image)
								{
									print '<td id = "single-image">';
									print '<a href="'.$image['url'].'" data-fancybox="gallery" data-caption="'.$image['caption'].'">';
									print '<img width:100px src = "'.$image['url'].'" alt = "'.get_field('photo_gallery_alt', $image['id']).'"></img></a>';
									print '<div><label id="single-image" class="title">'.$image['title'].'<label></div>';
									print '<div><label id="single-image caption">'.$image['caption'].'<label></div>';
									print '</td>'; 
								}
								?>
								</table>
								</div>						
								
							  <div id = "scarab_bibliography" class="single_scarab_field">
								<h2> Bibliography </h2>
								<?php print $scarab_info['Bibliography'];?>	
															  
							  </div>		
						
							
							
						   <?php if (!empty($scarab_info["3dmodel_obj"])) {
							   if (empty($scarab_info["3d_model_texture_file"]))
								   print "<h2> 3D Model - texure file not found! </h2>";
							   else { ?>
						   <div id = "scarab_3d" class="single_scarab_field">
								<h2> 3D Model - scroll to refresh </h2>  
								<style>
								#c {
								  width: 100%;
								  height: 100%;
								  display: block;
								  }
								  </style>                   
								  <canvas id="c"></canvas>
								<script>
								var obj_url = "<?php echo $scarab_info["3dmodel_obj"]["url"]?>"; 
								var texture_url = "<?php echo $scarab_info["3d_model_texture_file"]["url"]?>"; 
								</script>
								<script type="module" src="http://3.127.97.174/seal_db/wp-content/themes/ScarabDB/js/three_scarab.js&quot;"></script> 
						   </div>
						   <?php }}; 
					   do_action( 'ample_after_post_content' ); ?>
						</article>
						<?php if ( ( ample_option( 'ample_author_bio_setting', 0 ) == 1 ))  { ?>
							<div  class="author-box clearfix">
								<!--<div class="author-img"><?php echo get_avatar( get_the_author_meta( 'user_email' ), '100' ); ?></div>-->
								<div class="author-description-wrapper">
									<h4 class="author-name"><?php the_author_meta( 'first_name' ); ?> <?php the_author_meta( 'last_name' ); ?></h4>
									
									<p class="author-description"><?php the_author_meta( 'description' ); ?></p>
								</div>
							</div>
						<?php } ?>

						<?php if ( ample_option( 'ample_related_posts_setting', 0 ) == 1 ) {
							get_template_part( 'inc/related-posts' );
						} 
						comments_template('/comments.php',true);
					endwhile; ?>
				</div>
				<?php ample_both_sidebar_select(); ?>
			</div>

			<?php ample_sidebar_select(); ?>
		</div><!-- .inner-wrap -->
	</div><!-- .single-page -->

	<?php do_action( 'ample_after_body_content' );
get_footer(); ?>

